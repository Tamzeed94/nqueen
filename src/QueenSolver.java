import java.util.Scanner;


public class QueenSolver {

	int res=0;
	int sz;
	//int flag[][];
	int rowCounted=0;
	String resultString="";
	public QueenSolver()
	{
		rowCounted=0;
	}
	public void printFlag(int [][]flag)
	{
		for(int i=0;i<sz;i++)
			{
				for(int j=0;j<sz;j++)
					System.out.print(flag[i][j]+"	");
				System.out.println();
			}
	}
	public void reset()
	{
		rowCounted=0;
		res=0;
		resultString="";
		
	}
	public String toString(int []ar)
	{
		String ret="";
		for(int i=0;i<sz;i++)
		{
			ret+=ar[i]+" ";
			
		}
		ret+="\n";
		return ret;
		
	}
	
	public void printBoard(int []pos,int n)
	{
		for(int i=0;i<n;i++)
		{
			System.out.println(pos[i]);
			
		}
		
	}
	
	public boolean placeQueens(int []pos,int n,int i)
	{
	//	System.out.println("n i:"+n+" "+i);
		if(i==n)
		{
			res++;
			//System.out.println("printing solution; "+res);
			//printBoard(pos, n);
			//System.out.println("\n");
			resultString+=toString(pos);
			
			return true;		
		}
		for(int j=0;j<n;j++)
		{
			pos[i]=j;
			boolean ret,succ;
			ret=checkConstraint(pos,n,i);
			if(ret==false)
			{
				continue;
			}
			succ=placeQueens(pos, n, i+1);
			//if(succ==true) return true;
			
		}
		return false;		
	}
	public void printer(int [][]f)
	{
		for(int i=0;i<sz;i++)
		{
			for(int j=0;j<sz;j++)
				System.out.print(f[i][j]+" ");
			System.out.println();
			
		}
	}
	public void arrayCopier(int [][]source,int [][]dest)
	{
		for(int i=0;i<sz;i++)
		{
			for(int j=0;j<sz;j++)
				dest[i][j]=source[i][j];
		}
		
	}
//	
	public boolean placeQueensMrv(int []pos,int n,int [][]flag)
	{
		if(rowCounted==n)
		{
//			System.out.println("printing: "+res);
//			printBoard(pos, n);
//			System.out.println("\n");
			res++;
			//System.out.println("printing solution; "+res);
			//printBoard(pos, n);
			//System.out.println("\n");
			resultString+=toString(pos);
			return true;
		}
		int i=selectRowMrv(flag);
		if(i<0)
			return false;
		rowCounted++;
		for(int j=0;j<n;j++)
		{
		//	printer(flag);
			if(flag[i][j]!=-1)
			{
				pos[i]=j;
				int [][]fl=new int[n][n];
				arrayCopier(flag, fl);
				boolean f=forwardCheck(pos, i, j, fl);
				if(f==false)
				{
					continue;
					
				}
				
				boolean succ=placeQueensMrv(pos, n,fl);
			}
		}
		
		rowCounted--;
		return false;
	}
	

	
	public boolean checkConstraint(int []pos,int n,int r)
	{
		for(int i=0;i<r;i++)
		{
			if(pos[i]==pos[r] | pos[r]-(r-i)==pos[i]| pos[r]+(r-i)==pos[i])
			{
				return false;
			}
			//return true;
		}
		return true;
	}
	
	public boolean forwardCheck(int []pos,int i,int j,int [][]flag)
	{
		for(int k=0;k<sz;k++)
		{
			if(k!=i)
			{
				flag[k][j]=-1;
				int dif=Math.abs(k-i);
				int tmpIndex1=j+dif;
				int tmpIndex2=j-dif;
				if(tmpIndex2>=0)
					flag[k][tmpIndex2]=-1;
				if(tmpIndex1<sz)
					flag[k][tmpIndex1]=-1;
			}
			if(k==i)
			{
				for(int l=0;l<sz;l++)
					flag[i][l]=-1;
			}
			
		}
		return isAvailable(flag);
	}
	public boolean isAvailable(int [][]flag)
	{
		//System.out.println("row counted: "+rowCounted);
		if(rowCounted==sz)
			return true;
		for(int i=0;i<sz;i++)
		{
			for(int j=0;j<sz;j++)
			{
				if(flag[i][j]==0)
					return true;
			}
		}
		return false;
	}
	public int selectRowMrv(int [][]flag)
	{
		int ret=-1;
		int tempMax=Integer.MIN_VALUE;
		for(int i=0;i<sz;i++)
		{
			//System.out.println("i: "+i);
			int temp=0;
			for(int j=0;j<sz;j++)
			{
				if(flag[i][j]==-1)
					temp++;
			}
			if(temp!=sz)
			{
				if(temp>tempMax)
				{
					tempMax=temp;
					ret=i;
				}
				
			}
			
		}
		return ret;
	}
	public static void main(String args[])
	{
		Scanner scan=new Scanner(System.in);
		System.out.println("input: ");
		int n=scan.nextInt();
		QueenSolver solver=new QueenSolver();
		int []pos=new int[n];
		int [][]flag=new int [n][n];
		solver.sz=n;
		//solver.mark=new int[n];
		//solver.placeQueensMrv(pos, n,flag);
		System.out.println("NORMAL");
		solver.placeQueens(pos, n, 0);		
		System.out.println("number of solutions: "+solver.res);
		System.out.println(solver.resultString);
		solver.reset();
		
		System.out.println("WITH MRV");
		solver.placeQueensMrv(pos, n,flag);
		System.out.println("number of solutions: "+solver.res);
		System.out.println(solver.resultString);
		solver.reset();
		
	}
}
