import java.util.Scanner;


public class Solver {

	int res=0;
	int sz;
	//int flag[][];
	int mark[];
	int rowCounted=0;
	public Solver()
	{
		rowCounted=0;
	}
	public void printFlag(int [][]flag)
	{
		for(int i=0;i<sz;i++)
			{
				for(int j=0;j<sz;j++)
					System.out.print(flag[i][j]+"	");
				System.out.println();
			}
	}
	public void reset(int [][]flag)
	{
		//rowCounted=0;
		//java.util.Arrays.fill(flag, 0);
		rowCounted=0;
		for(int i=0;i<sz;i++)
		{
			mark[i]=0;
			for(int j=0;j<sz;j++)
				flag[i][j]+=1;
			
		}
	}
	public void printBoard(int []pos,int n)
	{
		for(int i=0;i<n;i++)
		{
			System.out.println(pos[i]);
			
		}
		
	}
	
	public boolean placeQueens(int []pos,int n,int i)
	{
	//	System.out.println("n i:"+n+" "+i);
		if(i==n)
		{
			res++;
			System.out.println("printing solution; "+res);
			printBoard(pos, n);
			System.out.println("\n");
			
			return true;		
		}
		for(int j=0;j<n;j++)
		{
			pos[i]=j;
			boolean ret,succ;
			ret=checkConstraint(pos,n,i);
			if(ret==false)
			{
				continue;
			}
			succ=placeQueens(pos, n, i+1);
			//if(succ==true) return true;
			
		}
		return false;		
	}
	public boolean checkMark()
	{
		for(int i=0;i<sz;i++)
		{
			if(mark[i]==-1|mark[i]==0)
			{
				return false;
				
			}
		}
		return true;
	}
//	
	public boolean placeQueensMrv(int []pos,int n,int [][]flag)
	{
		System.out.println("row:"+rowCounted);
		if(rowCounted==n)
		{
			res++;
			System.out.println("printing solution; "+res);
			printBoard(pos, n);
			System.out.println("\n");
		//	reset();
			
			
			return true;
		}
		int i= selectRowMrv(flag);
		if(i==-1)return false;
		rowCounted++;
		//System.out.println("i:"+i);
		//printFlag();
		for(int j=0;j<n;j++)
		{
			
			if(flag[i][j]!=-1)
			{
				System.out.println("j: "+i+" "+j);
				pos[i]=j;
				mark[i]=1;
				boolean f=forwardCheck(pos, i, j,flag);
			//	System.out.println("f: "+f);
				if(f==false)
				{
					continue;				
				}
				//rowCounted++;
				//mark[i]=1;
				int [][]fl=new int [n][n];
				for(int i1=0;i1<n;i1++)
				{
					for(int j1=0;j1<n;j1++)
						fl[i1][j1]=flag[i1][j1];
					
				}
				boolean succ=placeQueensMrv(pos, n,fl);
				reset(fl);
			}			
		}	
		rowCounted--;
		return false;		
	}
	

	
	public boolean checkConstraint(int []pos,int n,int r)
	{
		for(int i=0;i<r;i++)
		{
			if(pos[i]==pos[r] | pos[r]-(r-i)==pos[i]| pos[r]+(r-i)==pos[i])
			{
				return false;
			}
			//return true;
		}
		return true;
	}
	
	public boolean forwardCheck(int []pos,int i,int j,int [][]flag)
	{
		for(int k=0;k<sz;k++)
		{
			if(k!=i)
			{
				flag[k][j]=-1;
				int dif=Math.abs(k-i);
				int tmpIndex1=j+dif;
				int tmpIndex2=j-dif;
				if(tmpIndex2>=0)
					flag[k][tmpIndex2]-=1;
				if(tmpIndex1<sz)
					flag[k][tmpIndex1]-=1;
			}
			if(k==i)
			{
				for(int l=0;l<sz;l++)
					flag[i][l]=-1;
			}
			
		}
		return isAvailable(flag);
	}
	public boolean isAvailable(int [][]flag)
	{
		if(rowCounted==sz-1)
			return true;
		for(int i=0;i<sz;i++)
		{
			for(int j=0;j<sz;j++)
			{
				if(flag[i][j]==0)
					return true;
			}
		}
		return false;
	}
	public int selectRowMrv(int [][]flag)
	{
		int ret=-1;
		int max=Integer.MIN_VALUE;
		for(int i=0;i<sz;i++)
		{
			if(mark[i]==0)
			{
				int temp=0;
				for(int j=0;j<sz;j++)
				{
					if(flag[i][j]==-1)
						temp++;
				}
				if(temp!=sz)
				{
					if(temp>max)
					{
						max=temp;
						ret=i;
					}
					
				}
			
			}
				
		}
			
		return ret;
		
	}
	public static void main(String args[])
	{
		Scanner scan=new Scanner(System.in);
		System.out.println("input: ");
		int n=scan.nextInt();
		Solver solver=new Solver();
		int []pos=new int[n];
		int [][]flag=new int [n][n];
		solver.sz=n;
		solver.mark=new int[n];
	//	solver.reset();
		//solver.placeQueens(pos, n, 0);
		
		//unit testing chekForward
		
//		pos[0]=1;
//		System.out.println(solver.forwardCheck(pos, 2, 0));;
//		for(int i=0;i<n;i++)
//		{
//			for(int j=0;j<n;j++)
//				System.out.print(solver.flag[i][j]+"	");
//			System.out.println();
//		}
		
		///////////////////////////
		
		solver.placeQueensMrv(pos, n,flag);
		//solver.placeQueens(pos, n, 0);		
	}
}
